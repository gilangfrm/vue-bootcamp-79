import Vue from 'vue'
import VueRouter from 'vue-router'
import Router from 'vue-router'
import DashboardLayout from '../layout/DashboardLayout.vue'
import HomepageLayout from '../layout/HomepageLayout.vue'
// GeneralViews
import NotFound from '../pages/example/NotFoundPage.vue'

// Admin pages
import Overview from 'src/pages/example/Overview.vue'
import UserProfile from 'src/pages/example/UserProfile.vue'
import TableList from 'src/pages/example/TableList.vue'
import Typography from 'src/pages/example/Typography.vue'
import Icons from 'src/pages/example/Icons.vue'
import Maps from 'src/pages/example/Maps.vue'
import Notifications from 'src/pages/example/Notifications.vue'
import Upgrade from 'src/pages/example/Upgrade.vue'
import Latihan from 'src/pages/Latihan.vue'
import Latihan2 from 'src/pages/Latihan2.vue'
import Login from 'src/pages/Login.vue'
import LoginWithEmail from 'src/pages/LoginWithEmail.vue'
import LupaPassword from 'src/pages/LupaPassword.vue'
import Registrasi from 'src/pages/Registrasi.vue'
import Homepage from 'src/pages/Homepage.vue'
import Blankpage from 'src/pages/Blankpage.vue'
import DaftarBab from 'src/pages/DaftarBab.vue'
import DetailBab from 'src/pages/DetailBab.vue'

Vue.use(Router)

const router = new VueRouter({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/lupa-password',
      name: 'Lupa Password',
      component: LupaPassword,
    },
    {
      path: '/registrasi',
      name: 'Registrasi',
      component: Registrasi,
    },
    {
      path: '/login-with-email',
      name: 'Login With Email',
      component: LoginWithEmail,
    },
    {
      path: '/',
      component: HomepageLayout,
      meta: { requiresAuth: true },
      children: [
        {
          path: 'homepage',
          name: 'Homepage',
          component: Homepage
        },
        {
          path: 'daftar-bab/:id',
          name: 'Daftar Bab',
          component: DaftarBab
        },
        {
          path: 'detail-bab/:id',
          name: 'Detail Bab',
          component: DetailBab
        },
        {
          path: 'leaderboards',
          name: 'Leaderboards',
          component: Blankpage
        },
        {
          path: 'catatan-saya',
          name: 'Catatan Saya',
          component: Blankpage
        },
        {
          path: 'lihat-download',
          name: 'Lihat Download',
          component: Blankpage
        },
        {
          path: 'lihat-screenshoot',
          name: 'Lihat Screenshoot',
          component: Blankpage
        },
        {
          path: 'laporan-belajar',
          name: 'Laporan Belajar',
          component: Blankpage
        },
      ]
    },
    {
      path: '/latihan',
      name: 'Latihan',
      component: Latihan,
    },
    {
      path: '/',
      component: DashboardLayout,
      // meta: { requiresAuth: true },
      children: [
        {
          path: 'overview',
          name: 'Overview',
          component: Overview
        },
        {
          path: 'user',
          name: 'User',
          component: UserProfile
        },
        {
          path: 'table-list',
          name: 'Table List',
          component: TableList
        },
        {
          path: 'typography',
          name: 'Typography',
          component: Typography
        },
        {
          path: 'icons',
          name: 'Icons',
          component: Icons
        },
        {
          path: 'maps',
          name: 'Maps',
          component: Maps
        },
        {
          path: 'notifications',
          name: 'Notifications',
          component: Notifications
        },
        {
          path: 'latihan',
          name: 'Latihan',
          component: Latihan
        },
        {
          path: 'upgrade',
          name: 'Upgrade to PRO',
          component: Upgrade
        },
      ]
    },
    {
      path: '*',
      name: 'Login',
      redirect: '/login'
    }
  ]
})
router.beforeEach((to, from, next) => {
  console.log('to', to)
  if (to.matched.some(record => record.meta.requiresAuth) && !localStorage.getItem("sessionToken")) {
    next({
      path: '/login',
      query: {
        redirect: to.fullPath
      }
    });
  } else {
    next();
  }
});

export default router
